# Generated by Django 2.1.7 on 2019-03-02 14:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0004_auto_20190302_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friendlocation',
            name='friend',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='location', to=settings.AUTH_USER_MODEL),
        ),
    ]
